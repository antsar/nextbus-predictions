<?php

	ini_set('display_errors', false);
	
	/*
		Nextbus API Info: http://www.nextbus.com/xmlFeedDocs/NextBusXMLFeed.pdf
		List All Routes: http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=rutgers
		List All Stops for Route: http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&r=wknd2&a=rutgers
	*/
	
    // # of approaching buses to list
	$maxBuses = 5;
	
	
	//$stopID = 1023; // Davidson Hall
	
	$stops = array(
		1023, // Davidson Hall
		1007, // Busch Suites
	);
	
	$buses = array();
	foreach ($stops as $stopID) {
		$nextbusURL = "http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&a=rutgers&stopId={$stopID}";
		
		// fetch feed.. there's a cleaner way to write this but I don't have time
		if (@$xmlString = file_get_contents($nextbusURL)) {
			$xml = simplexml_load_string($xmlString);
		} else {
			// could not get feed
			$timenow = date("g:i:s A", time()-3);
			echo <<< ERROR_FETCHING_FEED
			<div class="bubbleTitle">NextBus Predictions as of {$timenow}</div>
			<table class="nextbusPredictions">
				<tr><td colspan=2 class='nobuses'>error fetching predictions</td></tr>
			</table>
ERROR_FETCHING_FEED;
			die();
		}
		
		foreach ($xml->predictions as $predictions) {
			if ($predictions->direction != null) {
				if ($predictions->direction->count() > 0) {
					foreach ($predictions->direction->prediction as $prediction) {
						$buses[(string)$prediction['seconds']] = array(
							"routeTitle" 	=> (string)$predictions['routeTitle'],
							"stopTitle" 	=> (string)$predictions['stopTitle'],
							"minutes"		=> (int)$prediction['minutes'],
							"seconds"		=> (int)$prediction['seconds'] - ((int)$prediction['minutes'] * 60),
							"layover"		=> (bool)$prediction['affectedByLayover'],
							"vehicle"		=> (int)$prediction['vehicle'],
						);
					}
				}
			}
		}

	}
    
	$time = time();
	echo "<div class=\"bubbleTitle\">NextBus Predictions as of " . date("g:i:s A", $time) . "</div>";
	echo '<table class="nextbusPredictions">';
	
	
	ksort($buses); // sort by how soon they're coming
		
	$buses = array_slice($buses, 0, $maxBuses);
	
	if (count($buses) > 0) {
		foreach ($buses as $bus) {
			
			if (strlen($bus['seconds']) == 1) $bus['seconds'] = "0{$bus['seconds']}";
			
			$timeClass = "busTime";
			
			$secondsTotal = $bus['seconds'] + ($bus['minutes']  * 60);
			
            // apply additional CSS classes to distinguish buses approaching soon
			if ($secondsTotal < 30) {
				$timeClass .= " lessThan30Seconds";
			} else if ($secondsTotal < 60) {
				$timeClass .= " lessThan60Seconds";
			} else if ($secondsTotal < 300) {
				$timeClass .= " lessThan5Minutes";
			} else {
				$timeClass .= " greaterThan5Minutes";
			}
			
            // Use an asterisk to indicate buses that are (or will be) waiting at a stop
            // before coming to your stop, meaning that the time estimate may be inaccurate.
            // This happens at RSC, BCC, etc..
			$layover = "";
			if ($bus['layover'] === true) $layover = "*";
			
			echo <<< ROUTE
				<tr><td>{$bus['routeTitle']}</td><td class="{$timeClass}">{$bus['minutes']}:{$bus['seconds']}{$layover}</td></tr>
ROUTE;
		}
	} else {
		echo "<tr><td colspan=2 class='nobuses'>no predictions available</td></tr>";
	}
	echo '</table>';
	
?>
